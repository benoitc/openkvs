openkvs
=====

key/value storage for Erlang applications that you can query locally 
and remotely in a transparent manner.

Features
--------

- concurrent readers and writers
- CAS semantic
- Atomic write operations (batch)
- transparent connection to remote or local storage using 
a simple URI.
- disk & memory storage using rocksdb
- optimised to handle high read and write load over the network
- a connection can be shared by multiple processes.
- multiple connections can co-exist in the same vm

Documentation
-------------

http://barrel-db.gitlab.io/openkvs-doc/overview.html


Build
-----

    $ rebar3 compile

Simple usage
------------

### Start a database 

On an erlang VM, run the following:

```erlang
> openkvs:start_db(test, []).
{ok,<0.579.0>,"openkvs://enlil-2:57593/test"}
```

### Connect locally to a database

You can now connect locally to it:

```erlang
> {ok, Conn} = openkvs:connect(test).
{ok,{openkvs_local,test}}
```

and make some queries:

```erlang
> openkvs:get(Conn, <<"a">>).
{error,not_found}
> openkvs:put(Conn, <<"a">>, <<"1">>, []).
{ok,<<"a">>,1478858999844787}
> openkvs:get(Conn, <<"a">>).
{ok,<<"1">>,1478858999844787}
```

### Connect remotly to a database

Starting a server, return in the logs the remote URI to connect. 
You can also use the function `openkvs:get_uri/1` to retrieve it.
The system of uri allows you to easily share a connection across 
the network and make your openkvs nodes discoverable.

```erlang
> openkvs:get_uri(test).
"openkvs://enlil-2:57593/test"
```

To connect remotly you then only need to use `openkvs:connect/2` with
this uri:

```erlang
> {ok, Conn} = openkvs:connect("openkvs://enlil-2:57593/test").
{ok,{openkvs_client,{'openkvs_client-1',test}}}
11:06:34.324 [info] teleport: client connected to peer-node 'openkvs_client-1'['openkvs-nonode@enlil-2:57593']
```

> Note: on the server side you should find such messages in the logs:
> `11:06:34.324 [info] teleport: server connected to peer node: 'openkvs_client-1'`

Then you can make some queries:

```erlang
>openkvs:put(Conn, <<"a">>, <<"2">>, []).
{error,conflict}
> openkvs:get(Conn, <<"a">>).
{ok,<<"1">>,1478858999844787}
> {ok, _V, Rev} = openkvs:get(Conn, <<"a">>).
{ok,<<"1">>,1478858999844787}
> openkvs:put(Conn, <<"a">>, <<"2">>, [{db_version, Rev}]).
{ok,<<"a">>,1478859529128211}
> openkvs:get(Conn, <<"a">>).
{ok,<<"2">>,1478859529128211}
```

### Configuration settings:

Example of a configuration file

```erlang
[
  {openkvs, [
    %% allows to start an openkvs server, disable it if yo only need 
    %% to run a peer
    {start_server, true}, 
    
    %% set the server port
    {port, 0},
    
    %% set the hostname
    {host, "localhost"}, 
    
    %% number of processes waiting for a connection
    {num_acceptors, 100},
    
    %% dbs to start initially
    {dbs, [
      {testdb, #{dir => "path/to/testdb"}}
    ]}
  ]}
].
```


Design
------

TODO

## Ownership and License

The contributors are listed in AUTHORS. This project uses the MPL v2
license, see LICENSE.

openkvs uses the [C4.1 (Collective Code Construction
Contract)](https://rfc.zeromq.org/spec:42/C4) process for contributions.

## Development

Under C4.1 process, you are more than welcome to help us by:

* join the discussion over anything from design to code style try out
* and [submit issue reports](https://gitlab.com/barrel-db/openkvs/issues/new)
* or feature requests pick a task in
* [issues](https://gitlab.com/barrel-db/openkvs/issues) and get it done fork
* the repository and have your own fixes send us pull requests and even
* star this project ^_^

To  run the test suite:

```
rebar3 ct --sname=test
```
