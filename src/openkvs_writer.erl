%% Copyright (c) 2016. Contributors as noted in the AUTHORS file
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

%% Created by benoitc on 06/10/2016.

-module(openkvs_writer).
-behaviour(gen_statem).

%% API
-export([
  start_link/3,
  put/4,
  delete/3,
  write_batch/2
]).


-export([
  unlocked/3,
  read_lock/3,
  write_lock/3
]).

-export([
  terminate/3,
  code_change/4,
  init/1,
  callback_mode/0
]).


put(Writer, Key, Value, Options) ->
  Res = put_1(get_db(Writer), Key, Value, Options),
  release_db(Writer),
  Res.

put_1(Db, Key, Value, Options) ->
  DbVersion = proplists:get_value(db_version, Options),
  NewVersion = proplists:get_value(new_version, Options),
  Force = proplists:get_value(force, Options, false),
  OldDoc = get_doc(Key, Db),
  IsConflict = check_conflict(OldDoc, DbVersion, Force),
  case IsConflict of
    false ->
      Version = case NewVersion of
                  undefined -> openkvs_lib:ts();
                  _ -> NewVersion
                end,
      NewDoc = #{val => Value, version => Version},
      case put_doc(Key, NewDoc, Options, Db) of
        ok -> {ok, Key, Version};
        true -> {ok, Key, Version};
        Error -> Error
      end;
    true -> {error, conflict}
  end.

delete(Writer, Key, Options) ->
  Res = delete_1(get_db(Writer), Key, Options),
  release_db(Writer),
  Res.

delete_1(Db, Key, Options) ->
  DbVersion = proplists:get_value(db_version, Options),
  Force = proplists:get_value(force, Options, false),
  OldDoc = get_doc(Key, Db),
  IsConflict = check_conflict(OldDoc, DbVersion, Force),
  case IsConflict of
    false ->
      case delete_doc(Key, Options, Db) of
        ok -> {ok, Key};
        true -> {ok, Key};
        Error -> Error
      end;
    true ->
      {error, conflict}
  end.

write_batch(Writer, Ops) ->
  Res = write_batch_1(suspend_before_batch(Writer), Ops),
  resume_after_batch(Writer),
  Res.

write_batch_1(Db, Ops) ->
  case prepare_batch(Ops, Db, []) of
    {ok, Ops2} ->
      do_write_batch(Ops2, Db);
    Error ->
      Error
  end.

do_write_batch(Ops, Db) ->
  openkvs_rocksdb:write_batch(Db, Ops).

prepare_batch([], _Db, Acc) ->
  {ok, lists:reverse(Acc)};
prepare_batch([{_BatchId, {_Seq, {delete, Key, Options}}} | Rest], Db, Acc) ->
  DbVersion = proplists:get_value(db_version, Options),
  Force = proplists:get_value(force, Options, false),
  OldDoc = get_doc(Key, Db),
  case check_conflict(OldDoc, DbVersion, Force) of
    true ->
      {error, conflict};
    false ->
      prepare_batch(Rest, Db, [{delete, Key} | Acc])
  end;
prepare_batch([{_BatchId, {_Seq, {put, Key, Val, Options}}}| Rest], Db, Acc) ->
  DbVersion = proplists:get_value(db_version, Options),
  NewVersion = proplists:get_value(new_version, Options),
  Force = proplists:get_value(force, Options, false),
  OldDoc = get_doc(Key, Db),
  case check_conflict(OldDoc, DbVersion, Force) of
    true ->
      {error, conflict};
    false ->
      Version = case NewVersion of
                  undefined -> openkvs_lib:ts();
                  _ -> NewVersion
                end,
      NewDoc = #{val => Val, version => Version},
      prepare_batch(Rest, Db, [{put, Key, NewDoc} | Acc])
  end;
prepare_batch(_, _, _) ->
  {error, invalid_op}.

check_conflict(OldDoc, DbVersion, Force) ->
  case {OldDoc, DbVersion} of
    {nil, undefined} -> false;
    {nil, _} -> true;
    {_Doc, undefined} when Force =:= true -> false;
    {Doc, DbVersion} ->
      #{ version := OldVersion } = Doc,
      if
        OldVersion /= DbVersion -> true;
        true -> false
      end
  end.

get_doc(Key, Db) ->
  case openkvs_rocksdb:get_doc(Db, Key) of
    {ok, Doc} -> Doc;
    {error, not_found} -> nil;
    Error -> throw(Error)
  end.

put_doc(Key, NewDoc, Options, Db) ->
  openkvs_rocksdb:put_doc(Db, Key, NewDoc, Options).

delete_doc(Key, Options, Db) ->
  openkvs_rocksdb:delete_doc(Db, Key, Options).

get_db(Writer) ->
  case robust_call(Writer, read_lock) of
     false -> get_db(Writer); % see do_write_unlock/2
     Db -> Db
  end.

release_db(Writer) ->  robust_call(Writer, read_unlock).

suspend_before_batch(Mgr) ->
   case robust_call(Mgr, write_lock) of
     false -> suspend_before_batch(Mgr);
     Db -> Db
   end.

resume_after_batch(Mgr) ->
  robust_call(Mgr, write_unlock).

robust_call(Writer, Req) ->
  robust_call(Writer, Req, 99). % (99+1)*100ms = 10s

robust_call(Writer, Req, 0) ->
  gen_statem:call(Writer, Req, infinity);
robust_call(Writer, Req, Retries) ->
  try
    gen_statem:call(Writer, Req, infinity)
  catch exit:{noproc, _} ->
    timer:sleep(100),
    robust_call(Writer, Req, Retries - 1)
  end.


start_link(Name, DbName, Backend) ->
  Tab = table_name(DbName),
  IsNew = create_table(Tab),
  gen_statem:start_link({local, Name},
                        ?MODULE,
                        [IsNew, Tab, Backend],
                        []).

init([IsNew, Tab, Backend]) ->
  {ok, Db} = openkvs_rocksdb:get_db(Backend),

  Writer = case IsNew of
    true -> nil;
    false ->
      [remonitor_reader(Tab, Reader) || Reader <- get_readers(Tab)],
      remonitor_writer(Tab, get_writer(Tab))
  end,

  Data =
    #{db => Db,
      tab => Tab,
      writer => Writer},

  {ok, unlocked, Data}.

callback_mode() -> state_functions.

terminate(_Reason, _State, #{ tab := Tab}) ->
  _ = ets:delete(Tab),
  ok.

code_change(_Vsn, State, Data, _Extra) -> {ok, State, Data}.


%% state callbacks
unlocked({call, From = {Pid, _}}, read_lock, Data = #{db:=Db, tab:=Tab}) ->
  MRef = erlang:monitor(process, Pid),
  add_reader(Tab, {Pid, MRef}),
  {next_state, read_lock, Data, [{reply, From, Db}]};
unlocked({call, {WPid, _Tag} = From}, write_lock, Data = #{db:=Db, tab:=Tab}) ->
  MRef = erlang:monitor(process, WPid),
  Data1 = Data#{ writer => set_writer(Tab, {From, MRef})},
  {next_state, write_lock, Data1, [{reply, From, Db}]};
unlocked(EventType, EventContent, Data) ->
  handle_event(EventType, unlocked, EventContent, Data).

read_lock({call, From = {Pid, _}}, read_lock, Data = #{db:=Db, tab:=Tab}) ->
  MRef = erlang:monitor(process, Pid),
  add_reader(Tab, {Pid, MRef}),
  {keep_state, Data, [{reply, From, Db}]};
read_lock({call, From = {Pid, _}}, read_unlock, Data = #{ tab := Tab}) ->
  case delete_reader(Tab, Pid) of
    {ok, MRef} ->
      erlang:demonitor(MRef, [flush]),
      case has_readers(Tab) of
        true ->
          {keep_state, Data, [{reply, From, ok}]};
        false ->
          {next_state, unlocked, Data, [{reply, From, ok}]}
      end;
    false ->
      lager:debug(
        "openkvs: read-unlock without holding read lock: ~p",
        [Pid]
      ),
      {keep_state, Data, [{reply, From, ok}]}
  end;
read_lock({call, _From}, write_lock, Data) ->
  {keep_state, Data, postpone};
read_lock(info,  {'DOWN', _MRef, _, Pid, _}, Data = #{ tab := Tab}) ->
  case delete_reader(Tab, Pid) of
    {ok, _MRef} ->
      case has_readers(Tab) of
        true ->
          {keep_state, Data};
        false ->
          {next_state, unlocked, Data}
      end;
    false ->
      {keep_state, Data}
  end;
read_lock(EventType, EventContent, Data) ->
  handle_event(EventType, read_lock, EventContent, Data).


write_lock({call, From = {WPid, _Tag}}, write_unlock, Data = #{ tab := Tab}) ->
  case Data of
    #{ writer := {{WPid, _Tag2}, MRef} } ->
      erlang:demonitor(MRef, [flush]),
      Data1 = Data#{writer => unset_writer(Tab)},
      {next_state, unlocked, Data1, [{reply, From, ok}]};
    _ ->
      lager:debug(
        "openkvs: write-unlock without holding write lock: ~p",
        [WPid]
      ),
      {keep_state, Data, [{reply, From, ok}]}
  end;
write_lock({call, _From}, read_lock, Data) ->
  {keep_state, Data, postpone};
write_lock({call, From={Pid, _}}, read_unlock, Data) ->
  lager:debug(
    "openkvs: read-unlock without holding read lock: ~p",
    [Pid]
  ),
  {keep_state, Data, [{reply, From, ok}]};

write_lock({call, _From}, write_lock, Data) ->
  {keep_state, Data, postpone};
write_lock(info, {'DOWN', MRef, _, Pid, _},
           Data = #{ tab := Tab, writer := {Pid, MRef} }) ->
  Data1 = Data#{writer => unset_writer(Tab)},
  {next_state, unlocked, Data1};
write_lock(EventType, EventContent, Data) ->
  handle_event(EventType, locked, EventContent, Data).

handle_event(_EventType, State, EventContent, Data) ->
  lager:warning(
    "openkvs: ~s unknown event ~s: ~p~n",
    [?MODULE_STRING, State, EventContent]
  ),
  {keep_state, Data}.


remonitor_reader(Tab, {Pid, _MRef}) ->
  MRef = erlang:monitor(process, Pid),
  ets:insert(Tab, {Pid, MRef}).

remonitor_writer(_Tab, nil) -> nil;
remonitor_writer(Tab, {Pid, _MRef}) ->
  MRef = erlang:monitor(process, Pid),
  Writer = {Pid, MRef},
  ets:insert(Tab, {'$writer', Writer}),
  Writer.

create_table(Tab) ->
  case ets:info(Tab, name) of
    undefined ->
      ets:new(Tab, [named_table, public, ordered_set, {keypos, 1},
                    {read_concurrency, false}, {write_concurrency, false}]),
      ets:insert(Tab, {'$writer', nil}),
      true;
    _ ->
      false
  end.

table_name(DbName) ->
  list_to_atom(?MODULE_STRING ++ "__writer_" ++ atom_to_list(DbName)).

set_writer(Tab, W) -> ets:insert(Tab, {'$writer', W}), W.
get_writer(Tab) -> ets:lookup_element(Tab, '$writer', 2).
unset_writer(Tab) -> ets:insert(Tab, {'$writer', nil}), nil.

add_reader(Tab, Reader= {Pid, _}) -> ets:insert(Tab, {Pid, Reader}).

delete_reader(Tab, Pid) ->
  case ets:lookup(Tab, Pid) of
    [{Pid, {_Pid, MRef}}] ->
      ets:delete(Tab, Pid),
      {ok, MRef};
    [] ->
      false
  end.

get_readers(Tab) ->
  ets:select(Tab, [{_MatchHead = {'$1', '$2'},
                    _Guards = [{is_pid, '$1'}],
                    _Result  = ['$_']}]).

has_readers(Tab) ->
  ets:info(Tab, size) > 1.
