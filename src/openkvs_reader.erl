%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

%% Created by benoitc on 02/10/2016.

-module(openkvs_reader).
-author("Benoit Chesneau").
-behaviour(gen_server).

%% API
-export([
  get_db/1
]).

-export([start_link/3]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(DEFAULT_READERS, 100).

%%%===================================================================
%%% Types
%%%===================================================================

-type state() :: #{}.

%%%===================================================================
%%% API
%%%===================================================================

get_db(Sup) ->
  wpool:call(Sup, get_db).


-spec start_link(atom(), atom(), proplists:proplists()) -> {ok, pid()}.
start_link(SupName, Backend, Options) ->
  PoolSize = maps:get(readers, Options, ?DEFAULT_READERS),
  WPoolConfigOpts = application:get_env(openkvs, wpool_opts, []),
  WPoolOptions = [
    {overrun_warning, 5000},
    {overrun_handler, {openkvs_lib, report_overrun}},
    {workers, PoolSize},
    {worker, {?MODULE, [Backend]}}
  ],
  wpool:start_pool(SupName, WPoolConfigOpts ++ WPoolOptions).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
%%%
-spec init(term()) -> {ok, state()}.
init([Backend]) ->
  {ok, Db} = openkvs_rocksdb:get_db(Backend),
  {ok, Db}.

-spec handle_call(term(), term(), state()) -> {reply, term(), state()}.
handle_call(get_db, _From, State) ->
  {reply, State, State};

handle_call(_Request, _From, State) ->
  {reply, bad_call, State}.

-spec handle_cast(term(), state()) -> {noreply, state()}.
handle_cast(_Msg, State) ->
  {noreply, State}.

-spec handle_info(term(), state()) -> {noreply, state()}.
handle_info(_Info, State) ->
  {noreply, State}.

-spec terminate(term(), state()) -> ok.
terminate(_Reason, _State) ->
  ok.

-spec code_change(term(), state(), term()) -> {ok, state()}.
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
