-module(openkvs_batch_sup).
-author("Benoit Chesneau").

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-spec start_link() -> {ok, pid()} | {error, term()}.
start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

-spec init(any()) ->
  {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
init([]) ->
  Spec =
    #{id => openkvs_batch,
      start => {openkvs_batch, start_link, []},
      restart => temporary,
      shutdown => 5000,
      type => worker,
      modules => [openkvs_batch]
    },
  {ok, {{simple_one_for_one, 10, 10}, [Spec]}}.
