%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

-module(openkvs_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
  DbSup =
    #{id => openkvs_db_sup,
      start => {openkvs_db_sup, start_link, []},
      restart => permanent,
      shutdown => infinity,
      type => supervisor,
      modules => [openkvs_db_sup]},

  BatchSup =
    #{id => openkvs_batch_sup,
      start => {openkvs_batch_sup, start_link, []},
      restart => permanent,
      shutdown => infinity,
      type => supervisor,
      modules => [openkvs_batch_sup]},

  Server = case application:get_env(openkvs, start_server, true) of
             true ->
               [#{id => openkvs_server,
                  start => {openkvs_server, start_server, []},
                  restart => permanent,
                  shutdown => 2000,
                  type => worker,
                  modules => [openkvs_server]}];
             false -> []
           end,

  {ok, { {one_for_one, 5, 10}, Server ++ [DbSup, BatchSup]} }.
