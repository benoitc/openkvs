-module(openkvs_server).
-author("Benoit Chesneau").

%% API
-export([
  start_server/0,
  server_name/0,
  db_uri/1
]).


start_server() ->
  teleport:start_server(server_name() , server_config()).


server_config() ->
  Host = application:get_env(openkvs, host, default_host()),
  Port = application:get_env(openkvs, port, 0),
  NumAcceptors = application:get_env(openkvs, num_acceptors, 100),
  Transport = application:get_env(openkvs, transport, tcp),
  
  #{host => Host,
    port => Port,
    transport => Transport,
    num_acceptors => NumAcceptors}.

default_host() ->
  [_, Host0] = string:tokens(atom_to_list(node()), "@"),
  case Host0 of
    "nohost" ->
      {ok, Host1} = inet:gethostname(),
      Host1;
    _ ->
      Host0
  end.

server_name() ->
  [SName, _] = string:tokens(atom_to_list(node()), "@"),
  teleport_lib:to_atom("openkvs-" ++ SName).


db_uri(DbName) ->
  Server = server_name(),
  #{host := Host, transport := Transport} = ranch:get_protocol_options(Server),
  Port = teleport_server_sup:get_port(Server),
  UriBin = iolist_to_binary(
    ["openkvs://", Host, ":", integer_to_list(Port), "/", atom_to_list(DbName),
      case Transport of
        ranch_ssl -> "?channel_encrypt=true";
        ranch_tcp -> ""
      end]
  ),
  binary_to_list(UriBin).
