%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

%% Created by benoitc on 02/10/2016.

-module(openkvs_lib).
-author("Benoit Chesneau").

%% API
-export([
  get_backend/1,
  backend_name/1,
  n_schedulers/0,
  ts/0,
  report_overrun/1,
  uniqid/0,
  ssl_conf/2,
  data_dir/0,
  to_atom/1,
  to_list/1,
  to_binary/1,
  get_database/1,
  config_from_uri/1
]).

-export([partial_chain/1]).

-include("openkvs.hrl").
-include_lib("public_key/include/OTP-PUB-KEY.hrl").

%% @doc get a storage backend module from its name
-spec get_backend(atom()) -> module().
get_backend(rocksdb) -> openkvs_rocksdb;
get_backend(ets) -> openkvs_ets;
get_backend(Mod) -> Mod.

backend_name(openkvs_ets) -> ets;
backend_name(openkvs_rocksdb) -> rocksdb;
backend_name(Mod) -> Mod.

-spec n_schedulers() -> integer().
n_schedulers() -> erlang:system_info(schedulers_online).

-spec ts() -> integer().
ts()->
  {Mega, Secs, Micro} = erlang:timestamp(),
  (Mega*1000000 + Secs)*1000000 + Micro.

-spec report_overrun(term()) -> ok.
report_overrun(Report) ->
  lager:debug("~p", [Report]).

-spec uniqid() -> binary().
uniqid() -> uuid:uuid_to_string(uuid:get_v4(), standard).

-spec data_dir() -> list().
data_dir() ->
  case application:get_env(openkvs, data_dir) of
    undefined ->
      Dir = filename:absname(lists:concat(["openkvs-", node()])),
      application:set_env(openkvs, data_dir, Dir),
      Dir;
    {ok, Dir} ->
      Dir
  end.

to_atom(V) when is_atom(V) -> V;
to_atom(V) when is_list(V) ->
  case catch list_to_existing_atom(V) of
    {'EXIT', _} -> list_to_atom(V);
    A -> A
  end;
to_atom(V) when is_binary(V) ->
  case catch binary_to_existing_atom(V, utf8) of
    {'EXIT', _} -> binary_to_atom(V, utf8);
    B -> B
  end;
to_atom(_) -> error(badarg).

to_list(V) when is_list(V) -> V;
to_list(V) when is_binary(V) -> binary_to_list(V);
to_list(V) when is_atom(V) -> atom_to_list(V);
to_list(V) when is_list(V) -> integer_to_list(V);
to_list(_) -> error(badarg).

to_binary(V) when is_binary(V) -> V;
to_binary(V) when is_list(V) -> list_to_binary(V);
to_binary(V) when is_atom(V) -> atom_to_binary(V, utf8);
to_binary(V) when is_integer(V) -> integer_to_binary(V);
to_binary(_) -> error(badarg).

-spec ssl_conf(client | server, inet:hostname() | inet:ip_address()) -> proplists:proplists().
ssl_conf(client, Host) ->
  TrustStore = application:get_env(openkvs, client_ssl_store, []),
  ExtraOpts0 = proplists:get_value(Host, TrustStore, []),
  DefaultOpts = lists:append(?SSL_DEFAULT_COMMON_OPTS, ?SSL_DEFAULT_CLIENT_OPTS),
  Insecure =  proplists:get_value(insecure, ExtraOpts0),
  CACerts = certifi:cacerts(),
  ExtraOpts = case Insecure of
                true -> [{verify, verify_none}];
                false ->
                  VerifyFun = {fun ssl_verify_hostname:verify_fun/3,  [{check_hostname, Host}]},
                  [
                    {cacerts, CACerts},
                    {partial_chain, fun partial_chain/1},
                    {verify_fun, VerifyFun} | ExtraOpts0
                  ]
              end,
  merge_opts(ExtraOpts, DefaultOpts);
ssl_conf(server, Host) ->
  TrustStore = application:get_env(openkvs, client_ssl_store, []),
  ExtraOpts = proplists:get_value(Host, TrustStore, []),
  DefaultOpts = lists:append(?SSL_DEFAULT_COMMON_OPTS, ?SSL_DEFAULT_SERVER_OPTS),
  merge_opts(ExtraOpts, DefaultOpts).

merge_opts(List1, List2) ->
  SList1 = lists:usort(fun props_compare/2, List1),
  SList2 = lists:usort(fun props_compare/2, List2),
  lists:umerge(fun props_compare/2, SList1, SList2).

props_compare({K1,_V1}, {K2,_V2}) -> K1 =< K2;
props_compare(K1, K2) -> K1 =< K2.


%% code from rebar3 under BSD license
partial_chain(Certs) ->
  Certs1 = lists:reverse([{Cert, public_key:pkix_decode_cert(Cert, otp)} ||
    Cert <- Certs]),
  CACerts = certifi:cacerts(),
  CACerts1 = [public_key:pkix_decode_cert(Cert, otp) || Cert <- CACerts],

  case find(fun({_, Cert}) ->
    check_cert(CACerts1, Cert)
            end, Certs1) of
    {ok, Trusted} ->
      {trusted_ca, element(1, Trusted)};
    _ ->
      unknown_ca
  end.

extract_public_key_info(Cert) ->
  ((Cert#'OTPCertificate'.tbsCertificate)#'OTPTBSCertificate'.subjectPublicKeyInfo).

check_cert(CACerts, Cert) ->
  lists:any(fun(CACert) ->
    extract_public_key_info(CACert) == extract_public_key_info(Cert)
            end, CACerts).

-spec find(fun(), list()) -> {ok, term()} | error.
find(Fun, [Head|Tail]) when is_function(Fun) ->
  case Fun(Head) of
    true ->
      {ok, Head};
    false ->
      find(Fun, Tail)
  end;
find(_Fun, []) ->
  error.

-spec get_database(map()) -> string().
get_database(#{ path := ""}) ->
  error(bad_uri);
get_database(#{ path := "/"}) ->
  error(bad_uri);
get_database(#{ path := Path}) ->
  [Db | _] = string:tokens(Path, "/"),
  list_to_atom(Db).

config_from_uri(Uri) ->
  Parsed = teleport_uri:parse(Uri),
  Transport = case maps:find(channel_encrypt, Parsed) of
                {ok, true} -> ssl;
                _ -> tcp
              end,
  Db =  get_database(Parsed),
  {Parsed#{transport => Transport}, Db}.
