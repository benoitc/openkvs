%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

%% Created by benoitc on 03/10/2016.

-module(openkvs_db_sup).
-author("Benoit Chesneau").

-behaviour(supervisor).

%% API
-export([
  start_link/0,
  db_spec/2
]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

-spec start_link() -> {ok, pid()} | {error, term()}.
start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

-spec init(any()) ->
  {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
init([]) ->
  Dbs = application:get_env(openkvs, dbs, []),
  Specs = lists:map(
    fun({Name, Config}) ->
      db_spec(Name, Config)
    end, Dbs),
  {ok, {{one_for_one, 10, 10}, Specs}}.


db_spec(Name, Config) ->
  #{id => Name,
    start => {openkvs_db, start_link, [Name, Config]},
    restart => transient,
    shutdown => 2000,
    type => worker,
    modules => [openkvs_db]
  }.
