%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

%% Created by benoitc on 02/10/2016.

-module(openkvs).
-author("Benoit Chesneau").

%% API
-export([
  start_db/2,
  stop_db/1,
  connect/1,
  close/1,
  get_uri/1
]).

%% crud api

-export([
  get/2,
  get_version/2,
  get_next/2,
  get_previous/2,
  get_first/1,
  get_last/1,
  get_keyrange/5,
  put/4,
  delete/3,
  start_batch/1,
  end_batch/2,
  cancel_batch/1,
  batch_put/4,
  batch_delete/3
]).

-type conn() :: term().
-type uri() :: string().
-type backend() :: rocksdb | ets | module().
-type transport() :: tcp | ssl.

-type db_option() ::
        {backend, backend()} |
        {readers, non_neg_integer()} |
        {writers, non_neg_integer()} |
        {num_acceptors, non_neg_integer()} |
        {transport, transport()} |
        {port, inet:port_number()} |
        {ip, inet:socket_address()}.

-type db_conf() :: [db_option()].
-type write_options() :: [{sync, boolean()}].
-type key() :: term().
-type val() :: term().
-type db_version() :: term().
-type range_options() :: [{startkey_inclusive, boolean()}
                          |{endkey_inclusive, boolean()}
                          | {reverse, boolean()}].
-type batch() :: pid().

%% @doc start a database and return its PID and URI
-spec start_db(atom(), db_conf()) -> {ok, pid(), uri()}.
start_db(Name, Config) when is_list(Config) ->
  start_db(Name, maps:from_list(Config));
start_db(Name, Config) ->
  case application:get_env(openkvs, start_server, true) of
    true ->
      Spec = openkvs_db_sup:db_spec(Name, Config),
      case supervisor:start_child(openkvs_db_sup, Spec) of
        {ok, Pid} ->
          {ok, Pid, openkvs_db:get_uri(Name)};
        {error, {already_started, Pid}} ->
          {ok, Pid, openkvs_db:get_uri(Name)};
        Error ->
          Error
      end;
    false ->
      {error, server_not_started}
  end.

%% @doc stop a database
stop_db(Name) ->
  case supervisor:terminate_child(openkvs_db_sup, Name) of
    ok ->
      _ = supervisor:delete_child(openkvs_db_sup, Name),
      ok;
    Error ->
      Error
  end.


%% @doc connect to a database. If the database is locale,
%% all connections are locales and we only use message passing.
-spec connect(teleport:uri() | atom()) -> {ok, conn()} | {error, term()} | undefined.
connect(Name) when is_atom(Name)->
  case whereis(Name) of
    undefined -> undefined;
    _Pid ->  {ok, {openkvs_local, Name}}
  end;
connect(Uri) when is_list(Uri)->
  case is_local(Uri) of
    {ok, Name} -> {ok, {openkvs_local, Name}};
    false ->
      ClientId =  openkvs_lib:to_atom(
        "openkvs_client-" ++ integer_to_list(erlang:unique_integer([monotonic, positive]))
      ),
      {Config, Db} = openkvs_lib:config_from_uri(Uri),
      true = teleport:connect(ClientId, Config),
      {ok, {openkvs_client, {ClientId, Db}}}
  end;
connect(_) ->
  erlang:error(badarg).


close({openkvs_local, _Name}) -> ok;
close({openkvs_client, {Name, _}}) -> teleport:disconnect(Name).

%% @doc return the database uri
-spec get_uri(DbName::atom()) -> URI::uri().
get_uri(Name) -> openkvs_db:get_uri(Name).


-spec get(conn(), key()) -> {ok, val(), db_version()} | {error, term()}.
get({Mod, State}, Key) ->
  Mod:get(State, Key).

-spec get_version(conn(), key()) -> {ok, val()} | {error, term()}.
get_version({Mod, State}, Key) ->
  Mod:get_version(State, Key).

%% @doc  operation takes a key and returns the value for the next key in the sorted set of keys.
%% Keys are sorted lexicographically by their byte representation.
-spec get_next(conn(), key()) -> {ok, key(), val(), db_version()} | {error, term()}.
get_next({Mod, State}, Key) ->
  Mod:next(State, Key).

%% @doc operation takes a key and returns the value for the previous key in the sorted set of keys.
%% Keys are sorted lexicographically by their byte representation.
-spec get_previous(conn(), key()) -> {ok, key(), val(), db_version()} | {error, term()}.
get_previous({Mod, State}, Key) ->
  Mod:prev(State, Key).

%% @doc return the value of the first key in the sorted set of keys.
-spec get_first(conn()) -> {ok, key(), val(), db_version()} | {error, term()}.
get_first({Mod, State}) ->
  Mod:first(State).

%% @doc return the value of the last key in the sorted set of keys.
-spec get_last(conn()) -> {ok, key(), val(), db_version()} | {error, term()}.
get_last({Mod, State}) ->
  Mod:last(State).

%% @doc takes a start and end key and returns all keys between those in the sorted set of keys.
%% This operation can be configured so that the range is either inclusive or exclusive of the
%% start and end keys, the range can be reversed, and the requester can cap the number of keys
%% returned. Queries `{ok, [{Key, Val}], Next}'. When Next is different from <<>>,
%% it can be used as the Start key to retrieve the following results in the range (ie. when max has been achieved).
-spec get_keyrange(Conn, Start, End, Max, Opts) -> Res when
  Conn :: conn(),
  Start :: key(),
  End :: key(),
  Max :: non_neg_integer(),
  Key :: key(),
  Val :: val(),
  Next :: key(),
  Opts :: range_options(),
  Res :: {ok, [{Key, Val}], Next} | {error, term()}.
get_keyrange({Mod, State}, Start, End, Max, Opts) ->
  Mod:keyrange(State, Start, End, Max, Opts).

%% @doc update or create a value, when a db_version i given to the option the version on disk is compared.
%% If both the versions are different a conflict is returned. Otherwise it's overwritten. This allows
%% simple optimistic update.
%% TODO: maybe the default would be to force the version?
-spec put(conn(), key(), val(), write_options()) -> {ok, key(), db_version()} | {error, term()}.
put({Mod, State}, Key, Value, Options)  when is_list(Options) ->
  Mod:put(State, Key, Value, Options).

%% @doc delete a value
-spec delete(conn(), key(), write_options()) -> {ok, key(), db_version()} | {error, term()}.
delete({Mod, State}, Key, Options) when is_list(Options)->
  Mod:delete(State, Key, Options).

%% @doc start a batch operation
%% Batch Operation allows a group of K/V commands (PUT and DELETE) to perform all at once.
%% The commands within a batch are committed to the persistent store if all commands can be
%% committed or otherwise nothing is committed.
%%
%% All (PUT/DELETE) operations within a batch do not responses.
-spec start_batch(conn()) -> {ok, batch()}.
start_batch(Conn={Mod, State}) ->
  BatchId = openkvs_lib:uniqid(),
  case Mod:start_batch(State, BatchId) of
    {ok, BatchId} -> {ok, {Conn, BatchId}};
    Error -> error(Error)
  end.

%% @doc end batch operation and tell the number of expected operations
-spec end_batch(Batch::batch(), Count::non_neg_integer()) -> ok | {error, term()}.
end_batch({{Mod, State}, BatchId}, Count) ->
  Mod:end_batch(State, BatchId, Count).

%% @doc cancel a batch operation
-spec cancel_batch(batch()) -> ok | {error, term()}.
cancel_batch({{Mod, State}, BatchId}) ->
  Mod:cancel_batch(State, BatchId).

%% @doc put a KEY/VALUE to a database via a batch operation
-spec batch_put(batch(), key(), val(), write_options()) -> ok.
batch_put({{Mod, State}, BatchId}, Key, Value, Options) when is_list(Options)->
  Mod:batch_append(State, BatchId, {seq(), {put, Key, Value, Options}}).

%% @doc delete a KEY via a database operation
-spec batch_delete(batch(), key(), write_options()) -> ok.
batch_delete({{Mod, State}, BatchId}, Key, Options) when is_list(Options) ->
  Mod:batch_append(State, BatchId, {seq(), {delete, Key, Options}}).


%%====================================================================
%% Internal functions
%%====================================================================

is_local(Uri) ->
  case catch gproc:lookup_value({n, l, {openkvs_uri, Uri}}) of
    {'EXIT', _} -> false;
    Name -> {ok, Name}
  end.

seq() -> erlang:unique_integer([positive, monotonic]).

