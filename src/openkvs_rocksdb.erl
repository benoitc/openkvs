%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

%% Created by benoitc on 02/10/2016.

-module(openkvs_rocksdb).
-author("Benoit Chesneau").
-behavior(gen_server).

%% API
-export([
  get_value/2,
  get_version/2,
  get_doc/2,
  put_doc/4,
  delete_doc/3,
  first/1,
  last/1,
  next/2,
  prev/2,
  keyrange/5,
  write_batch/2
]).

%% backend callbacks
-export([
  start_link/3,
  get_db/1
]).


%% gen_server callbacks
-export([
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3
]).

-include("openkvs.hrl").

-define(DEFAULT_MAX, 16#ffffFFFFffffFFFF).

%%%===================================================================
%%% Types
%%%===================================================================

-type state() :: #{}.

%%%===================================================================
%%% API
%%%===================================================================

get_value(Db, Key) ->
  case get_doc(Db, Key) of
    {ok, #{ val := Val, version := Version}} ->
      {ok, Val, Version};
    Error ->
      Error
  end.

get_version(Db, Key) ->
  case get_doc(Db, Key) of
    {ok, #{ version := Version}} -> {ok, Version};
    Error -> Error
  end.

get_doc(Db, Key) ->
  case erocksdb:get(Db, Key, []) of
    {ok, BinDoc} ->
      Doc = decode_value(BinDoc),
      {ok, Doc};
    not_found -> {error, not_found};
    Error -> Error
  end.

put_doc(Db, Key, Doc, Options) ->
  Sync = proplists:get_value(sync, Options, false),
  BinDoc = encode_value(Doc),
  erocksdb:put(Db, Key, BinDoc, [{sync, Sync}]).

delete_doc(Db, Key, Options) ->
  Sync = proplists:get_value(sync, Options, false),
  erocksdb:delete(Db, Key, [{sync, Sync}]).

next(Db, Key) ->
  with_iterator(Db, fun(Itr) ->
    case erocksdb:iterator_move(Itr, Key) of
      {ok, Key, _} ->
        case erocksdb:iterator_move(Itr, next) of
          {ok, K, V} ->
            ok_result(K, V);
          _ ->
            {error, not_found}
        end;
      {ok, K, V} ->
        ok_result(K, V);
      {error, invalid_iterator} ->
        case erocksdb:iterator_move(Itr, first) of
          {ok, K, V} ->
            if
              Key < K ->
                ok_result(K, V);
              true ->
                {error, not_found}
            end
        end;
      {error, _} ->
        {error, not_found}
    end
  end).

prev(Db, Key) ->
  with_iterator(Db, fun(Itr) ->
    case erocksdb:iterator_move(Itr, Key) of
      {ok, Key, _} ->
        case erocksdb:iterator_move(Itr, prev) of
          {ok, K, V} ->
            ok_result(K, V);
          _ ->
            {error, not_found}
        end;
      {error, invalid_iterator} ->
        case erocksdb:iterator_move(Itr, last) of
          {ok, K, V} ->
            if
              Key > K ->
                ok_result(K, V);
              true ->
                {error, not_found}
            end
        end;
      _ ->
        {error, not_found}
    end
  end).

first(Db) ->
  with_iterator(Db,
    fun(Itr) ->
      case erocksdb:iterator_move(Itr, first) of
        {ok, K, V} -> ok_result(K, V);
        _ -> {error, not_found}
      end
    end).

last(Db) ->
  with_iterator(Db,
    fun(Itr) ->
      case erocksdb:iterator_move(Itr, last) of
        {ok, K, V} -> ok_result(K, V);
        _ -> {error, not_found}
      end
    end).

keyrange(Db, Start, End, Max, Opts) ->
  AccFun = fun(K, V, Acc) -> [{K, V} | Acc] end,
  Res = fold(Db, AccFun, [], Start, End, Max, Opts),
  case Res of
    {ok, AccOut, Next} ->
      {ok, lists:reverse(AccOut), Next};
    _ ->
      Res
  end.

write_batch(Db, Batch) ->
  Ops = prepare_batch(Batch),
  erocksdb:write(Db, Ops, [{sync, true}]).

%% we need to encode keys and value first
prepare_batch(Ops) ->
  Fun = fun
          ({delete, Key}) ->
            {delete, Key};
          ({put, Key, Value}) ->
            {put, Key, encode_value(Value)}
        end,
  lists:map(Fun, Ops).

ok_result(Key, Value) ->
  Doc = decode_value(Value),
  #{ val := Val, version := DbVersion} = Doc,
  {ok, Key, Val, DbVersion}.

with_iterator(Db, Fun) ->
  case (catch erocksdb:iterator(Db, [])) of
    {ok, Itr} ->
      try Fun(Itr)
      after
        _ = (catch erocksdb:iterator_close(Itr))
      end;
    Error ->
      Error
  end.


fold(Db, Fun, Acc, StartKey, EndKey, Max, Opts) ->
  StartKeyInclusive = proplists:get_value(startkey_inclusive, Opts, false),
  EndKeyInclusive = proplists:get_value(endkey_inclusive, Opts, false),
  Reverse = proplists:get_value(reverse, Opts, false),

  {Start, End, Dir} = case Reverse of
          true -> {EndKey, StartKey, prev};
          false -> {StartKey, EndKey, next}
        end,

  with_iterator(Db,
    fun(Itr) ->
      case {erocksdb:iterator_move(Itr, Start), Dir} of
        {{error, invalid_iterator}, prev} ->
          fold_1(erocksdb:iterator_move(Itr, last), Fun, Acc, Max, EndKeyInclusive, Dir, End, Itr);
        {{error, _}, _ } ->
          {ok, Acc, <<>>};
        {{ok, K, _V}, next} when K =:= Start, StartKeyInclusive =:= false ->
          fold_1(erocksdb:iterator_move(Itr, next), Fun, Acc, Max, EndKeyInclusive, Dir, End, Itr);
        {{ok, K, _V}, prev} when K =:= Start, EndKeyInclusive =:= false ->
          fold_1(erocksdb:iterator_move(Itr, prev), Fun, Acc, Max, StartKeyInclusive, Dir, End, Itr);
        {OK, next} ->
          fold_1(OK, Fun, Acc, Max, EndKeyInclusive, Dir, End, Itr);
        {OK, prev} ->
          fold_1(OK, Fun, Acc, Max, StartKeyInclusive, Dir, End, Itr)
      end
    end ).

fold_1({error, _}, _Fun, Acc, _Max, _EndKeyInclusive, _Dir, _End, _Itr) ->
  {ok, Acc, <<>>};
fold_1({ok, K, V}, Fun, Acc, _Max, true, _Dir, K, _Itr) ->
  {ok, Fun(K, fold_v(V), Acc), <<>>};
fold_1({ok, K, _V}, _Fun, Acc, _Max, false, _Dir, K, _Itr) ->
  {ok, Acc, <<>>};
fold_1({ok, K, _V}, _Fun, Acc, _Max, _EndKeyInclusive, next, End, _Itr) when K >= End ->
  {ok, Acc, <<>>};
fold_1({ok, K, _V}, _Fun, Acc, _Max, _EndKeyInclusive, prev, End, _Itr) when K =< End ->
  {ok, Acc, <<>>};
fold_1({ok, K, V}, Fun, Acc, Max, EndKeyInclusive, Dir, End, Itr) ->
  Acc1 = Fun(K, fold_v(V), Acc),
  Max1 = Max - 1,
  if
    Max1 > 0 ->
      fold_1(erocksdb:iterator_move(Itr, Dir), Fun, Acc1, Max1, EndKeyInclusive, Dir, End, Itr);
    true ->
      %% get next key
      Next = case erocksdb:iterator_move(Itr, Dir) of
               {ok, K, _V} -> K;
               _ -> <<>>
             end,
      {ok, Acc1, Next}
  end.

fold_v(ValBin) ->
  Doc = decode_value(ValBin),
  #{val := Val, version := Version} = Doc,
  {Val, Version}.


%% private

encode_value(Map) ->
  ValBin = maps:get(val, Map),
  Ver = maps:get(version, Map),
  VerBin = openkvs_lib:to_binary(Ver),
  << (byte_size(ValBin)):32/integer, ValBin/bytes,
     (byte_size(VerBin)):32/integer, VerBin/bytes >>.

decode_value(Bin) ->
  << ValLen:32/integer, Val:ValLen/bytes,
     VerLen:32/integer, Ver:VerLen/bytes >> = Bin,
  Version = binary_to_integer(Ver),
  #{val => binary:copy(Val), version => Version}.

%%%===================================================================
%%% backend callbacks
%%%===================================================================

get_db(Backend) -> gen_server:call(Backend, get_db).

start_link(Backend, Name, Config) ->
  gen_server:start_link({local, Backend}, ?MODULE, [Name, Config], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
%%%
-spec init(term()) -> {ok, state()}.
init([Name, Conf]) ->
  process_flag(trap_exit, true),
  %% get db dir
  DbDir = maps:get(dir, Conf, default_dir(Name)),
  _ = filelib:ensure_dir(DbDir),
  %% set open options
  OpenOpts = case maps:get(in_memory, Conf, false) of
    true -> [{create_if_missing, true}, {in_memory, true}];
    false -> [{create_if_missing, true}]
  end,
  {ok, Db} = erocksdb:open(DbDir, OpenOpts, []),
  {ok, #{name => Name, db=>Db, dir => DbDir}}.

-spec handle_call(term(), term(), state()) -> {reply, term(), state()}.
handle_call(get_db, _From, State = #{db := Db}) ->
  {reply, {ok, Db}, State}.

-spec handle_cast(term(), state()) -> {noreply, state()}.
handle_cast(_Msg, State) ->
  {noreply, State}.

-spec handle_info(term(), state()) -> {noreply, state()}.
handle_info(_Info, State) ->
  {noreply, State}.

-spec terminate(term(), state()) -> ok.
terminate(_Reason, #{ db := Db }) ->
  _ = erocksdb:close(Db),
  ok.

-spec code_change(term(), state(), term()) -> {ok, state()}.
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.


default_dir(Name) ->
  filename:join(openkvs_lib:data_dir(), atom_to_list(Name)).
