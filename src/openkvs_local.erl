%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

%% Created by benoitc on 02/10/2016.

-module(openkvs_local).
-author("Benoit Chesneau").

%% API
-export([
  get/2,
  get_version/2,
  next/2,
  prev/2,
  first/1,
  last/1,
  put/4,
  delete/3,
  keyrange/5,
  start_batch/2,
  end_batch/3,
  cancel_batch/2,
  batch_append/3
]).

-include("openkvs.hrl").

get(Db, Key) ->
  db_apply(Db, get_value, [Key]).

get_version(Db, Key) ->
  db_apply(Db, get_version, [Key]).

next(Db, Key) ->
  db_apply(Db, next, [Key]).

prev(Db, Key) ->
  db_apply(Db, prev, [Key]).

first(Db) ->
  db_apply(Db, first, []).

last(Db) ->
  db_apply(Db, last, []).

keyrange(Db, Start, End, Max, Opts) ->
  db_apply(Db, keyrange, [Start, End, Max, Opts]).

put(Db, Key, Value, Options) ->
  Writer = openkvs_db:get_writer(Db),
  openkvs_writer:put(Writer, Key, Value, Options).

delete(Db, Key, Options) ->
  Writer = openkvs_db:get_writer(Db),
  openkvs_writer:delete(Writer, Key, Options).


db_apply(Db, Fun, Args) ->
  Reader = openkvs_db:get_reader(Db),
  DbRes = openkvs_reader:get_db(Reader),
  erlang:apply(openkvs_rocksdb, Fun, [DbRes] ++ Args).

start_batch(Db, BatchId) ->
  case openkvs_batch:has_batch(Db, BatchId) of
    true -> {error, conflict};
    false->
      {ok, _Pid} = openkvs_batch:start_batch(Db, BatchId),
      {ok, BatchId}
  end.

end_batch(Db, BatchId, Count) ->
  openkvs_batch:end_batch(Db, BatchId, Count).

cancel_batch(Db, BatchId) ->
  openkvs_batch:stop_batch(Db, BatchId),
  ok.

batch_append(Db, BatchId, Op) ->
  case openkvs_batch:has_batch(Db, BatchId) of
    true ->
      openkvs_batch:append(Db, BatchId, {BatchId, Op});
    false ->
      {error, {batch_not_found, Db, BatchId}}
  end.
