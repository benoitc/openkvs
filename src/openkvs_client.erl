%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

%% Created by benoitc on 02/10/2016.

-module(openkvs_client).
-author("Benoit Chesneau").

%% API
-export([
  get/2,
  get_version/2,
  next/2,
  prev/2,
  first/1,
  last/1,
  keyrange/5,
  put/4,
  delete/3,
  start_batch/2,
  end_batch/3,
  cancel_batch/2,
  batch_append/3
]).

-include("openkvs.hrl").

get({Name, Db}, Key) ->
  teleport:call(Name, openkvs_local, get, [Db, Key]).

get_version({Name, Db}, Key) ->
  teleport:call(Name, openkvs_local, get_version, [Db, Key]).

next({Name, Db}, Key) ->
  teleport:call(Name, openkvs_local, next, [Db, Key]).

prev({Name, Db}, Key) ->
  teleport:call(Name, openkvs_local, prev, [Db, Key]).

first({Name, Db}) ->
  teleport:call(Name, openkvs_local, first, [Db]).

last({Name, Db}) ->
  teleport:call(Name, openkvs_local, last, [Db]).

keyrange({Name, Db}, Start, End, Max, Opts) ->
  teleport:call(Name, openkvs_local, keyrange, [Db, Start, End, Max, Opts]).

put({Name, Db}, Key, Value, Options) ->
  teleport:call(Name, openkvs_local, put, [Db, Key, Value, Options]).

delete({Name, Db}, Key, Options) ->
  teleport:call(Name, openkvs_local, delete, [Db, Key, Options]).

start_batch({Name, Db}, BatchId) ->
  teleport:call(Name, openkvs_local, start_batch, [Db, BatchId]).

end_batch({Name, Db}, BatchId, Count) ->
  teleport:call(Name, openkvs_local, end_batch, [Db, BatchId, Count]).

cancel_batch({Name, Db}, BatchId) ->
  teleport:call(Name, openkvs_local, cancel_batch, [Db, BatchId]).

batch_append({Name, Db}, BatchId, Op) ->
  teleport:cast(Name, openkvs_local, batch_append, [Db, BatchId, Op]).
