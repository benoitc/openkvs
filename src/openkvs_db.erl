%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

%% Created by benoitc on 02/10/2016.

-module(openkvs_db).
-author("Benoit Chesneau").

-behaviour(supervisor).

%% API
-export([
  start_link/2,
  get_writer/1,
  get_reader/1
]).

%% Supervisor callbacks
-export([init/1]).

-export([
  writer_name/1,
  batch_table_name/1,
  find_db/1,
  get_uri/1
]).


-include("openkvs.hrl").

%%%===================================================================
%%% API functions
%%%===================================================================

-spec start_link(atom(), proplists:proplists()) -> {ok, pid()} | {error, term()}.
start_link(Name, Config) ->
  supervisor:start_link({local, Name}, ?MODULE, [Name, Config]).

get_writer(Name) ->
  case find_db(Name) of
    undefined -> throw(no_db);
    #{writer := W} -> W
  end.

get_reader(Name) ->
  reader_sup_name(Name).


%%%===================================================================
%%% db functions
%%%===================================================================


writer_name(Name) ->
  to_atom(?MODULE_STRING ++ [$-|atom_to_list(Name)] ++ "-writer").

batch_table_name(Name) ->
  to_atom("openkvs__batch_" ++ atom_to_list(Name)).

get_uri(Name) ->
  case find_db(Name) of
    undefined -> undefined;
    #{uri := Uri} -> Uri
  end.

find_db(Name) ->
  case catch gproc:lookup_value({n, l, {openkvs_db, Name}}) of
    {'EXIT', _} -> undefined;
    Db -> Db
  end.

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

-spec init(any()) ->
  {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
init([Name, Config]) ->
  Backend = backend_name(Name),
  Writer = writer_name(Name),
  ReaderSup = reader_sup_name(Name),
  Batch = batch_table_name(Name),

  _ = create_batch_table(Batch),

  URI = openkvs_server:db_uri(Name),

  Db = #{
    name => Name,
    uri => URI,
    writer => Writer,
    backend => Backend,
    reader_sup => ReaderSup,
    t_batch => Batch,
    config => Config
  },

  gproc:reg({n, l, {openkvs_uri, URI}}, Name),
  gproc:reg({n, l, {openkvs_db, Name}}, Db),

  BackendSpec = #{
    id => Backend,
    start => {openkvs_rocksdb, start_link, [Backend, Name, Config]},
    restart => permanent,
    shutdown => 2000,
    type => worker,
    modules => [openkvs_rocksdb]
  },

  WriterSpec = #{
    id => Writer,
    start => {openkvs_writer, start_link, [Writer, Name, Backend]},
    restart => permanent,
    shutdown => 5000,
    type => worker,
    modules => [openkvs_writer]
  },

  ReaderSupSpec = #{
    id => ReaderSup,
    start => {openkvs_reader, start_link, [ReaderSup, Backend, Config]},
    restart => permanent,
    shutdown => 5000,
    type => worker,
    modules => [openkvs_reader]
  },
  
  SupIntensity = maps:get(transactor_sup_intensity, Config, 5),
  SupPeriod = maps:get(transactor_sup_period, Config, 60),
  SupStrategy = {one_for_all, SupIntensity, SupPeriod},

  {ok, {SupStrategy, [BackendSpec, WriterSpec, ReaderSupSpec]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

backend_name(Name) ->
  list_to_atom(?MODULE_STRING ++ [$-|atom_to_list(Name)] ++ "-backend").

reader_sup_name(Name) ->
  list_to_atom(?MODULE_STRING ++ [$-|atom_to_list(Name)] ++ "-reader-sup").

create_batch_table(Name) ->
  _ = ets:new(
    Name,
    [public, named_table, bag,
     {read_concurrency, true},
     {write_concurrency, true}]),
  ok.

to_atom(V) ->
  openkvs_lib:to_atom(V).
