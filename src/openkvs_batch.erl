%% Copyright (c) 2016 Contributors as noted in the AUTHORS file
%%
%% This file is part openkvs, The Barrel KeyValue database engine in Erlang
%%
%% This Source Code Form is subject to the terms of the Mozilla Public
%% License, v. 2.0. If a copy of the MPL was not distributed with this
%% file, You can obtain one at http://mozilla.org/MPL/2.0/.

-module(openkvs_batch).
-author("benoitc").

-behaviour(gen_server).

%% API
-export([
  start_link/2,
  start_batch/2,
  has_batch/2,
  append/3,
  end_batch/3,
  stop_batch/2
]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API
%%%===================================================================

start_batch(Db, BatchId) ->
  supervisor:start_child(openkvs_batch_sup, [Db, BatchId]).

stop_batch(Db, BatchId) ->
  case gproc:where(batch_key(Db, BatchId)) of
    undefined -> ok;
    Pid -> supervisor:terminate_child(openkvs_batch_sup, Pid)
  end.

end_batch(Db, BatchId, Count) ->
  call(Db, BatchId, {end_batch, Count}).

has_batch(Db, BatchId) ->
  case gproc:where(batch_key(Db, BatchId)) of
    undefined ->
      false;
    _ ->
      true
  end.

append(Db, BatchId, Op) ->
  call(Db, BatchId, {append, Op}).

start_link(Db, BatchId) ->
  gen_server:start_link(via(Db, BatchId), ?MODULE, [Db, BatchId], []).

%%%===================================================================

batch_key(Db, BatchId) -> {n, l, {openkvs_batch, Db, BatchId}}.

via(Db, BatchId) ->
  {via, gproc, batch_key(Db, BatchId)}.

call(Db, BatchId, Req) -> gen_server:call(via(Db, BatchId), Req).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([Db, BatchId]) ->
  Timeout = application:get_env(openkvs, batch_timeout, 30000),
  TRef = erlang:send_after(Timeout, self(), timeout),
  {ok, #{ tref => TRef, db => Db, batchid => BatchId,  operations => []}}.

handle_call({append, Op}, _From, #{operations := Operations}=State) ->
  State2 = State#{operations => [Op|Operations]},
  case should_commit(State2) of
    true ->
      #{ from := From } = State,
      Res = commit(State2),
      gen_server:reply(From, Res),
      {stop, end_batch, Res, State2};
    false ->
      {reply, ok, State2}
  end;

handle_call({end_batch, _Count}, _From, #{ count  := _N }=State) ->
  {stop, end_batch, {error, invalid_batch}, State};
handle_call({end_batch, Count}, From, #{operations := Operations}=State) ->
  Len = length(Operations),
  if
    Count =:= Len  ->
      Res = commit(State),
      {stop, end_batch, Res, State};
    Len < Count ->
      State2 = State#{ count => Count, from => From},
      {noreply, State2};
    true ->
      {stop, end_batch, {error, invalid_batch}, State}
  end;

handle_call(Request, _From, State) ->
  {reply, {bad_call, Request}, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(timeout, State) ->
  {stop, normal, State};

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, #{tref := TRef}) ->
  erlang:cancel_timer(TRef),
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.


should_commit(#{ operations := Ops, count := N }) when N =:= length(Ops) -> true;
should_commit(_) -> false.

commit(#{ db := Db, operations := Operations0}) ->
  #{writer := Writer} = openkvs_db:find_db(Db),
  Operations1 = lists:sort(
                  fun({SeqA, _}, {SeqB, _}) when SeqA < SeqB -> true;
                     (_, _) -> false
                  end, Operations0),
  openkvs_writer:write_batch(Writer, Operations1).
