%% Copyright 2016, Bernard Notarianni
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(openkvs_SUITE).
-author("Bernard Notarianni").

%% API
-export(
   [ all/0
   , groups/0
   , init_per_suite/1
   , end_per_suite/1
   , init_per_group/2
   , end_per_group/2
   , init_per_testcase/2
   , end_per_testcase/2
   ]).

-export(
   [ basic_operations/1
   , next_prev_keyrange/1
   , concurent_reads/1
   , concurent_writes/1
   , concurrent_next_prev/1
   , batch_basic/1
   , batch_lock/1
   , run_on_slave_start_db/2
   , run_on_slave_stop_db/0
   ]).

all() ->
  [ {group, local_storage_in_memory}
  , {group, local_storage_on_disk}
  , {group, remote_storage_in_memory}
  , {group, remote_storage_on_disk}
  ].

groups() ->
  [ {local_storage_in_memory,  [sequence], tests()}
  , {local_storage_on_disk,    [sequence], tests()}
  , {remote_storage_in_memory, [sequence], tests()}
  , {remote_storage_on_disk,   [sequence], tests()}
  ].

tests() ->
  [ basic_operations
  , next_prev_keyrange
  , concurent_reads
  , concurent_writes
  , concurrent_next_prev
  , batch_basic
  , batch_lock
  ].

%% =============================================================================

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(openkvs),
  Config.

end_per_suite(Config) ->
  Config.

init_per_group(local_storage_in_memory, Config) ->
  [{server, local}, {in_memory, true}|Config];
init_per_group(local_storage_on_disk, Config) ->
  [{server, local}, {in_memory, false}|Config];

init_per_group(remote_storage_in_memory, Config) ->
  {ok, RemoteNode} = start_slave(slave_mem),
  timer:sleep(100),
  [{server, remote}, {in_memory, true}, {remote_node, RemoteNode}|Config];
init_per_group(remote_storage_on_disk, Config) ->
  {ok, RemoteNode} = start_slave(slave_disk),
  timer:sleep(100),
  [{server, remote}, {in_memory, false}, {remote_node, RemoteNode}|Config].

end_per_group(remote_storage_in_memory, Config) ->
  ok = stop_slave(slave_mem),
  Config;
end_per_group(remote_storage_on_disk, Config) ->
  ok = stop_slave(slave_disk),
  Config;
end_per_group(_Group, Config) ->
  Config.

init_per_testcase(_Test, Config) ->
  InMemory = proplists:get_value(in_memory, Config, false),
  DbDir = filename:join(proplists:get_value(data_dir, Config), "test"),

  StartOpts = case InMemory of
    false -> [{dir, DbDir}];
    true -> [{in_memory, true}]
  end,

  case proplists:get_value(server, Config) of
    local ->
      {ok, _Pid, Uri} = openkvs:start_db(test, StartOpts),
      [{uri, Uri}, {db_dir, DbDir}|Config];
    remote ->
      RemoteNode = proplists:get_value(remote_node, Config),
      {ok, Uri} = start_remote_db(RemoteNode, DbDir, InMemory),
      [{uri, Uri}, {db_dir, DbDir}|Config]
  end.

end_per_testcase(_Test, Config) ->
  case proplists:get_value(server, Config) of
    local ->
      ok = openkvs:stop_db(test);
    remote ->
      RemoteNode = proplists:get_value(remote_node, Config),
      ok = stop_remote_db(RemoteNode)
  end,
  InMemory = proplists:get_value(in_memory, Config, false),
  case InMemory of
    false ->
      DbDir = proplists:get_value(db_dir, Config),
      os:cmd("rm -rf " ++ DbDir),
      ok;
    true ->
      ok
  end,
  ok.

%% =============================================================================
%% Vanilla operations
%% =============================================================================

basic_operations(Config) ->
  Uri = proplists:get_value(uri, Config),
  {ok, Conn} = openkvs:connect(Uri),
  {error, not_found} = openkvs:get(Conn, <<"a">>),
  {ok, <<"a">>, Rev} = openkvs:put(Conn, <<"a">>, <<"1">>, []),
  {error, conflict} = openkvs:put(Conn, <<"a">>, <<"1">>, [{db_version, 1}]),
  {ok, <<"a">>, Rev2} = openkvs:put(Conn, <<"a">>, <<"1">>, [{db_version, Rev}]),
  {ok, <<"a">>} = openkvs:delete(Conn, <<"a">>, [{db_version, Rev2}]),
  ok = openkvs:close(Conn),
  ok.

next_prev_keyrange(Config) ->
  Uri = proplists:get_value(uri, Config),
  {ok, Conn} = openkvs:connect(Uri),
  Revs = add_docs(Conn, [ {<<"a">>, <<"1">>}
                        , {<<"b">>, <<"2">>}
                        , {<<"c">>, <<"3">>}
                        ]),
  {ok, <<"1">>, Va} = openkvs:get(Conn, <<"a">>),
  {ok, <<"b">>, <<"2">>, Vb} = openkvs:get_next(Conn, <<"a">>),
  {ok, <<"c">>, <<"3">>, Vc} = openkvs:get_next(Conn, <<"b">>),
  {error, not_found} = openkvs:get_next(Conn, <<"c">>),
  {error, not_found} = openkvs:get_previous(Conn, <<"a">>),
  {ok, <<"a">>, <<"1">>, Va} = openkvs:get_previous(Conn, <<"b">>),
  {ok, <<"b">>, <<"2">>, Vb} = openkvs:get_previous(Conn, <<"c">>),
  {ok, [{<<"b">>, {<<"2">>, Vb}}, {<<"c">>, {<<"3">>, Vc}}], <<>>} =
     openkvs:get_keyrange(Conn, <<"a">>, <<"c">>, 10, [{endkey_inclusive, true}]),
  {ok, <<"a">>, <<"1">>, Va} = openkvs:get_first(Conn),
  {ok, <<"c">>, <<"3">>, Vc} = openkvs:get_last(Conn),

  3 = delete_docs(Conn, Revs),
  ok = openkvs:close(Conn),
  ok.

add_docs(Conn, KeyValues) ->
  lists:foldl(fun({K,V}, Acc) ->
                  {ok, K, Rev} = openkvs:put(Conn, K, V, []),
                  [{K,Rev}|Acc]
              end, [], KeyValues).

delete_docs(Conn, KeyRevs) ->
  lists:foldl(fun({K,R}, Acc) ->
                  {ok, K} = openkvs:delete(Conn, K, [{db_version, R}]),
                  Acc+1
              end, 0, KeyRevs).

%% =============================================================================
%% Concurrent processing
%% =============================================================================

concurent_reads(Config) ->
  Uri = proplists:get_value(uri, Config),
  {ok, Conn} = openkvs:connect(Uri),
  {ok, <<"a">>, Rev} = openkvs:put(Conn, <<"a">>, <<"1">>, []),

  Parent = self(),
  FunRead = fun() ->
                Result = openkvs:get(Conn, <<"a">>),
                Parent ! {reply, self(), Result}
            end,

  NumberParallelReads = 200,
  [ spawn_link(FunRead) || _ <- lists:seq(1, NumberParallelReads) ],

  Results = collect_replies(NumberParallelReads),
  [ {ok, <<"1">>, Rev} = R || R <- Results ],
  {ok, <<"a">>} = openkvs:delete(Conn, <<"a">>, [{db_version, Rev}]),
  ok = openkvs:close(Conn),
  ok.

%% =============================================================================


concurent_writes(Config) ->
  Uri = proplists:get_value(uri, Config),
  {ok, Conn} = openkvs:connect(Uri),

  NumberParallelWrites = 500,
  NumberSequenceWrites = 20,
  Parent = self(),
  FunWrite =
    fun(P) ->
        DocName = doc_name(P),
        LastVer = lists:foldl(fun(N, OldVer) ->
                                  PutOpts = case OldVer of
                                    undefined -> [];
                                    _ -> [{db_version, OldVer}]
                                  end,
                                  V = integer_to_binary(N),
                                  {ok, DocName, Ver} = openkvs:put(Conn, DocName, V, PutOpts),
                                  Ver
                              end, undefined, lists:seq(1,NumberSequenceWrites)),
        Parent ! {reply, self(), {DocName, LastVer}}
    end,

  [ spawn_link(fun() -> FunWrite(P) end) || P <- lists:seq(1, NumberParallelWrites) ],
  Results = collect_replies(NumberParallelWrites),

  lists:foreach(fun({DocName, Version}) ->
                    {ok, ValBin, _} = openkvs:get(Conn, DocName),
                    NumberSequenceWrites = binary_to_integer(ValBin),
                    {ok, DocName} = openkvs:delete(Conn, DocName,
                                                      [{db_version, Version}])
                end, Results),
  ok = openkvs:close(Conn),
  ok.



%% =============================================================================

concurrent_next_prev(Config) ->
  Uri = proplists:get_value(uri, Config),
  {ok, Conn} = openkvs:connect(Uri),
  NumberOfDocs = 200,
  Docs = lists:map(fun(N) ->
                       {doc_name(N), integer_to_binary(N)}
                   end, lists:seq(1, NumberOfDocs)),
  Revs = add_docs(Conn, Docs),

  Parent = self(),
  FunWalk = fun() ->
                ok = forward(Conn, 1, NumberOfDocs-1),
                ok = backward(Conn, NumberOfDocs, 2),
                ok = scan_range(Conn, 1, NumberOfDocs),
                Parent ! {reply, self(), ok}
            end,

  NumberParallelWalks = 50,
  [ spawn_link(FunWalk) || _ <- lists:seq(1, NumberParallelWalks) ],

  Results = collect_replies(NumberParallelWalks),
  [ ok = R || R <- Results ],

  NumberOfDocs = delete_docs(Conn, Revs),
  ok = openkvs:close(Conn),
  ok.

forward(_Conn, _Start, 0) ->
  ok;
forward(Conn, Start, Steps) ->
  {ok, NextKey, NextValue, _} = openkvs:get_next(Conn, doc_name(Start)),
  NextKey = doc_name(Start+1),
  NextValue = integer_to_binary(Start+1),
  forward(Conn, Start+1, Steps-1).

backward(_Conn, _Start, 0) ->
  ok;
backward(Conn, Start, Steps) ->
  {ok, NextKey, NextValue, _} = openkvs:get_previous(Conn, doc_name(Start)),
  NextKey = doc_name(Start-1),
  NextValue = integer_to_binary(Start-1),
  forward(Conn, Start-1, Steps-1).

scan_range(Conn, Start, End) ->
  StartKey = doc_name(Start),
  EndKey = doc_name(End),
  {ok, Range, <<>>} = openkvs:get_keyrange(
    Conn, StartKey, EndKey,  End-Start, [{endkey_inclusive, true}]
  ),
  lists:foreach(fun({K,{Val, Ver}}) ->
                    {ok, Val, Ver} = openkvs:get(Conn, K)
                end, Range),
  ok.

%% =============================================================================
%% Batch
%% =============================================================================

batch_basic(Config) ->
  Uri = proplists:get_value(uri, Config),
  {ok, Conn} = openkvs:connect(Uri),
  {ok, <<"doc">>, Ver} = openkvs:put(Conn, <<"doc">>, <<"0">>, []),

  {ok, Batch} = openkvs:start_batch(Conn),
  ok = openkvs:batch_put(Batch, <<"a">>, <<"1">>, []),
  ok = openkvs:batch_put(Batch, <<"a">>, <<"4">>, []),
  ok = openkvs:batch_put(Batch, <<"b">>, <<"2">>, []),
  ok = openkvs:batch_delete(Batch, <<"doc">>, [{db_version, Ver}]),
  ok = openkvs:batch_put(Batch, <<"c">>, <<"3">>, []),
  ok = openkvs:end_batch(Batch, 5),

  {error, not_found} = openkvs:get(Conn, <<"doc">>),
  {ok, <<"4">>, _} = openkvs:get(Conn, <<"a">>),
  {ok, <<"3">>, _} = openkvs:get(Conn, <<"c">>),
  ok = openkvs:close(Conn),
  ok.

batch_lock(Config) ->
  Uri= proplists:get_value(uri, Config),
  {ok, Conn} = openkvs:connect(Uri),
  {ok, <<"a">>, Va} = openkvs:put(Conn, <<"a">>, <<"0">>, []),
  {ok, <<"b">>, Vb} = openkvs:put(Conn, <<"b">>, <<"0">>, []),

  {ok, Batch} = openkvs:start_batch(Conn),
  ok = openkvs:batch_put(Batch, <<"a">>, <<"1">>, [{db_version, Va}]),
  ok = openkvs:batch_put(Batch, <<"b">>, <<"1">>, [{db_version, Vb}]),

  {ok, _, _} = openkvs:put(Conn, <<"a">>, <<"9">>, [{db_version, Va}]),

  {error, conflict} = openkvs:end_batch(Batch, 2),

  {ok, <<"9">>, _} = openkvs:get(Conn, <<"a">>),
  {ok, <<"0">>, _} = openkvs:get(Conn, <<"b">>),
  ok = openkvs:close(Conn),
  ok.

%% =============================================================================
%% Helpers for creation of remote connections
%% =============================================================================

start_remote_db(HostNode, DbDir, InMemory) ->
  {ok_from_slave, Uri} = rpc:call(HostNode, ?MODULE, run_on_slave_start_db, [DbDir, InMemory]),
  {ok, Uri}.

stop_remote_db(HostNode) ->
  ok_from_slave = rpc:call(HostNode, ?MODULE, run_on_slave_stop_db, []),
  ok.

run_on_slave_start_db(DbDir, InMemory) ->
  StartOpts = case InMemory of
    false -> [{dir, DbDir}];
    true -> [{in_memory, true}]
  end,
  {ok, _Pid, Uri} = openkvs:start_db(test, StartOpts),
  {ok_from_slave, Uri}.

run_on_slave_stop_db() ->
  ok = openkvs:stop_db(test),
  ok_from_slave.

start_slave(Node) ->
  {ok, HostNode} = ct_slave:start(Node,
                                  [{kill_if_fail, true}, {monitor_master, true},
                                   {init_timeout, 3000}, {startup_timeout, 3000}]),
  pong = net_adm:ping(HostNode),
  CodePath = filter_rebar_path(code:get_path()),
  true = rpc:call(HostNode, code, set_path, [CodePath]),
  {ok,_} = rpc:call(HostNode, application, ensure_all_started, [openkvs]),
  ct:print("\e[32m ---> Node ~p [OK] \e[0m", [HostNode]),
  {ok, HostNode}.

stop_slave(Node) ->
  {ok, _} = ct_slave:stop(Node),
  ok.

%% a hack to filter rebar path
%% see https://github.com/erlang/rebar3/issues/1182
filter_rebar_path(CodePath) ->
  lists:filter(fun(P) ->
                  case string:str(P, "rebar3") of
                    0 -> true;
                    _ -> false
                  end
               end, CodePath). 

%% =============================================================================
%% Helpers
%% =============================================================================

doc_name(N) ->
  DocName = "doc" ++ string:right(integer_to_list(N), 4, $0),
  list_to_binary(DocName).

collect_replies(N) ->
  collect_replies(N, []).

collect_replies(0, Replies) ->
  Replies;
collect_replies(N, Replies) ->
  receive
    {reply, _From, Reply} ->
      collect_replies(N-1, [Reply|Replies])
  after 5000 ->
      ct:fail("parallel process timeout")
  end.
